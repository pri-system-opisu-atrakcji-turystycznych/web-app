import { Component } from '@angular/core';
import { AreasStoreService } from './modules/areas/areas-store.service';
import { MessagesStore } from './modules/messages/messages.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isCollapsed = true;
  currentArea$ = this.areasStore.currentArea$

  constructor(private areasStore: AreasStoreService, private messagesStore: MessagesStore) {}
}
