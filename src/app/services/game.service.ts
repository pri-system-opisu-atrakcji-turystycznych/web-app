import { Injectable } from '@angular/core';
import { RoutesService } from '../modules/routes/routes.service';
import { Observable, combineLatest, Subject, of, BehaviorSubject, zip } from 'rxjs';
import { GoelocationService } from './goelocation.service';
import { switchMap, take, map, debounceTime } from 'rxjs/operators';
import { latLng } from 'leaflet'
import { deepDistinct } from './utils.service';
import { AreaStorageService } from './area-storage.service';
import { TouristElementsService } from '../modules/tourist-elements/tourist-elements.service';
import { createNewGame, continueGame, createNotStartedGame } from './game.utils.service';
import { GameProgress } from './game.model';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  events = new Subject<'gameStarted'>();
  selectedRouteName = new Subject<string>();
  gameProgress = new BehaviorSubject<GameProgress>(createNotStartedGame());
  distanceToReach = 50;

  constructor(
    private routesService: RoutesService,
    private elementsService: TouristElementsService,
    private geolocationService: GoelocationService,
    private areaStorage: AreaStorageService
  ) {
    this.areaStorage.load<GameProgress>('gameProgress').subscribe(data => {
      if(data) { this.geolocationService.enable().subscribe() }
      if(data !== null) this.gameProgress.next(data)
    });

    combineLatest(
      this.gameProgress,
      this.geolocationService.coords$,
    ).pipe(deepDistinct()).subscribe(([progress, coords]) => {
      if(progress.status == 'onTheWay') {
        progress.distanceToTarget = coords.position
          .distanceTo(latLng(+progress.targetElement.gpsX, +progress.targetElement.gpsY));
        progress.status = progress.distanceToTarget <= this.distanceToReach ? 'targetReached' : 'onTheWay'
        this.gameProgress.next(progress);
      }
    });

    this.gameProgress.pipe(debounceTime(500),
      switchMap(progress => this.areaStorage.save('gameProgress', progress)
    )).subscribe();

    this.selectedRouteWithFirstElement.subscribe(([route, element]) => {
      this.gameProgress.next(createNewGame(route, element));
      console.log('gameStarted', route, element)
      this.events.next('gameStarted')
    });
  }

  startGame(routeName: string) {
    this.geolocationService.enable().subscribe(_ => {
      this.selectedRouteName.next(routeName);
    });
  }

  get progress$(): Observable<GameProgress> {
    return this.gameProgress.asObservable();
  }

  get events$(): Observable<string> {
    return this.events.asObservable();
  }

  private get selectedRouteWithFirstElement() {
    const route$ = this.selectedRouteName.pipe(
      switchMap(routeName => this.routesService.getRouteDetail(routeName))
    )
    const element$ = route$.pipe(
      switchMap(route => this.elementsService.getElementDetail(route.elements[0].name))
    )
    return zip(route$, element$)
  }

  private updateTargetElement = switchMap(
    (progress: GameProgress) => { 
      if(progress.status == 'completed') return of({...progress, targetElement: null})
      return this.elementsService.getElementDetail(progress.route.elements[progress.targetIndex].name).pipe(
        map(element => ({...progress, targetElement: element}))
    )}
  )

  continue(testAnswer: string = null) {
    this.gameProgress.pipe(
      take(1),
      map(progress => continueGame(progress, testAnswer)),
      this.updateTargetElement
    ).subscribe(progress => {
      this.gameProgress.next(progress);
    });
  }

  endGame() {
    this.gameProgress.next(createNotStartedGame());
  }
}
