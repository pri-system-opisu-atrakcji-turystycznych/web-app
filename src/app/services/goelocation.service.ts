import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { LatLng, latLng } from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class GoelocationService {
  private coords = new ReplaySubject<ApproximatePosition>(1);
  private subscription: Subscription;
  private geolocationObservable: Observable<Coordinates> = Observable.create(obs => {
    let id = navigator.geolocation.watchPosition(position => {
      obs.next(position.coords);
    }, error => {
      obs.error(error);
    })
    return _ => {
      navigator.geolocation.clearWatch(id)
    };
  })

  get coords$() {
    return this.coords.asObservable();
  }

  setCoords(coords: ApproximatePosition) {
    this.coords.next(coords);
  }

  enable(): Observable<ApproximatePosition> {
    
    if(!this.subscription || (!!this.subscription && this.subscription.closed)) {
      return Observable.create(obs => {
        this.subscription = this.geolocationObservable.subscribe(coords => {
          const approximatePosition = {
            position: latLng(coords.latitude, coords.longitude), 
            radius: coords.accuracy/2
          }
          this.coords.next(approximatePosition);
          obs.next(approximatePosition);
          obs.complete();
        }, error => {
          obs.error(error);
        });
      })
    } else return this.coords.asObservable().pipe(take(1));
  }
  
}

export interface ApproximatePosition {
  position: LatLng;
  radius: number;
}


