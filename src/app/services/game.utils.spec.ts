import { continueGame, createNewGame } from './game.utils.service';
import { Question } from '../modules/tourist-elements/tourist-element-model';
import * as lodash from 'lodash';

describe('continueGame', () => {
    const question = {question: '', answers: ['1', '2','3','4'], rightAnswer: '1'};
    const elementQuestion: Question = {
        id: 0,   
        content: 'nextQuestion',
        answerOne: '1',
        answerTwo: '2',
        answerTree: '3',
        rightAnswer: '4'
    };

    const gameDuringTest = {
        ...createNewGame(null, null),
        question: question,
        status: <any>'test',
        targetElement: <any>{question: elementQuestion},
        route: <any>{elements: [{}, {}]}
    }

    const gameAfterTargetReached = {
        ...gameDuringTest,
        status: <any>'targetReached'
    }

    it('adds points when answer is correct', () => {
        const gameProgress1 = continueGame(gameDuringTest, '1');
        expect(gameProgress1.points).toBe(1);
    });

    it('doesnt add points when answer is incorrect', () => {
        const gameProgress2 = continueGame(gameDuringTest, '2');
        expect(gameProgress2.points).toBe(0);
    });

    it('generates next question with shuffled answers', () => {
        const originalSuffleFun = lodash.shuffle;
        (<any>lodash.shuffle) = (([a1, a2, a3, a4]) => [a4, a3, a2, a1]);
        const gameProgress1 = continueGame(gameDuringTest, '1');
        expect(gameProgress1.question.question).toBe('nextQuestion');
        expect(gameProgress1.question.answers).toEqual(['4', '3', '2', '1']);
        (<any>lodash.shuffle) = originalSuffleFun;
    });

    it('increment target index', () => {
        const gameProgress1 = continueGame(gameDuringTest, '1');
        expect(gameProgress1.targetIndex).toBe(1);
    });

    it('completes game when there is no next element', () => {
        const gameProgress1 = continueGame({...gameDuringTest, targetIndex: 1}, '1');
        expect(gameProgress1.targetIndex).toBe(1);
        expect(gameProgress1.status).toBe('completed');
        expect(gameProgress1.targetElement).toBe(null);
    });

    it('comes to test when target is reached', () => {
        const gameProgress1 = continueGame(gameAfterTargetReached, null);
        expect(gameProgress1.status).toBe('test');
    })

    it('comes to test when target is reached', () => {
        const gameProgress1 = continueGame(gameAfterTargetReached, null);
        expect(gameProgress1.status).toBe('test');
    });

    it('increment target index when first target is reached', () => {
        const gameProgress1 = continueGame({...gameAfterTargetReached, question: null}, null);
        expect(gameProgress1.targetIndex).toBe(1);
    });
});