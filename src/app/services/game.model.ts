import { Route } from '../modules/routes/route-model';
import { TouristElement } from '../modules/tourist-elements/tourist-element-model';

export interface GameProgress {
    route: Route
    targetElement: TouristElement
    distanceToTarget: number
    targetIndex: number
    points: number
    question: GameQuestion
    status: 'notStarted' | 'onTheWay' | 'targetReached' | 'test' | 'completed';
}
  
export interface GameQuestion {
    question: string;
    answers: string[];
    rightAnswer: string;
}