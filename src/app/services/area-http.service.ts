import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyHttpService } from './my-http.service';
import { AreasStoreService } from '../modules/areas/areas-store.service';
import { Observable } from 'rxjs';
import { tap, filter, switchMap, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AreaHttpService {
  apiAdress = '';

  constructor(
    private http: HttpClient, 
    private areasStore: AreasStoreService
  ) {
    this.areasStore.currentArea$.subscribe(area => {
      this.apiAdress = area ? area.url : '';
    })
  }

  get<T>(path: string = '', params: any = {}): Observable<T> {
    return this.areasStore.currentArea$.pipe(
      filter(area => area != null),
      take(1),
      map(area => area.url),
      switchMap(url => this.http.get<T>(url + path, {params: params})),
      tap(data => this.generatePhotoUrl(data))
    )
  }

  generatePhotoUrl(obj) {
    if(Array.isArray(obj)) {
      obj.forEach(e => this.generatePhotoUrl(e));
    }

    if(obj && typeof obj === 'object' && obj['photo'])
      obj['photoUrl'] = `${this.apiAdress}image/${obj['photo']}`;
  }
}
