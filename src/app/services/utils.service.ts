import { shareReplay, distinctUntilChanged, tap, map } from 'rxjs/operators';
import { isEqual } from 'lodash';

export function normalStr(str: string): string {
  return str ? str.replace(new RegExp(" ", 'g'), "_") : null
}

export function head(a: Array<any>): any {
  return a.length > 0 ? a[0] : null;
}

export function normalizeNames() {
  return tap<Array<any>>(array => array.forEach(e => e.normalizedName = normalStr(e.name)))
}

export function findByName<T = any>(name: string) {
  return map<Array<any>, T>(array => array.find(e => e.normalizedName === normalStr(name) ))
}

export const cache = function<T>() {
  return shareReplay<T>({refCount: true})
}

export const deepDistinct = function<T>() {
  return distinctUntilChanged<T>((prev, curr) => isEqual(prev, curr))
}