import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MyHttpService {
  protected apiAdress = 'http://150.254.78.178:9001/adminserver/';
  
  constructor(private http: HttpClient) { }

  get<T>(path: string = '', params: any = {}): Observable<T> {
    return this.http.get<T>(this.getFullPath(path), {params: params})
  }

  private getFullPath(path: string) {
    if(path.startsWith("http"))
      return path
    else 
      return this.apiAdress + path
  }
}
