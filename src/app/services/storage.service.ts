

export interface StoragePath {
  storagePath: string;
}

export function storage() {
  return function(target: any, propertyName: string) {

    let _val = target[propertyName];
    let _pattern = undefined;
    let _init = false; 
  
    const getter = function() {
      _init = true;
      let temp = safeParse(localStorage.getItem(`${this.storagePath}_${propertyName}`));
      if(validate(temp, _pattern))
        _val = temp;
      else 
        localStorage.removeItem(`${this.storagePath}_${propertyName}`);
      return _val;
    };
  
    const setter = function(newVal) {
      _val = newVal;
      if(_init)
        localStorage.setItem(`${this.storagePath}_${propertyName}`, JSON.stringify(newVal))
      else
        _pattern = copy(newVal);
      _init = true;
    };
  
    if (delete target[propertyName]) {
      Object.defineProperty(target, propertyName, {
        get: getter,
        set: setter,
        configurable: true
      });
    }
  }
}

function validate(value: any, pattern: any): boolean {
  if(typeof value === typeof pattern) {
    if(typeof pattern === 'object') {
      for(let key of Object.keys(pattern)) {
        if(!validate(value[key], pattern[key])) return false
      };
      return true;
    }
    return true;
  } else return false;
}

function copy(a) {
  return safeParse(JSON.stringify(a));
}

export function safeParse(str: string) {
  try {
    return JSON.parse(str)
  } catch {
    return undefined;
  }
}
