import { TouristElement, Question } from '../modules/tourist-elements/tourist-element-model'
import { latLng } from 'leaflet';
import { GameService } from './game.service';
import { of } from 'rxjs';

const elementQuestion: Question = {
    id: 0,   
    content: 'nextQuestion',
    answerOne: '1',
    answerTwo: '2',
    answerTree: '3',
    rightAnswer: '4'
};

const testElement: TouristElement = {
    id: 1,
    name: "testElement",
    gpsX: "1",
    gpsY: "2",
    shortDescription: "",
    description: "",
    tip: null,
    question: elementQuestion,
    normalizedName: "",
    photoUrl: ""
}

const testRoute = {
    name: "testRoute",
    elements: [testElement, testElement]
}

const routesServiceMock: any = {
    getRouteDetail: (name) => testRoute
}

const elementsServiceMock: any = {
    getElementDetail: (name) => testElement
}

const geolocationServiceMock: any = {
    enable: () => {},
    coords$: of(latLng(0,0))
}

let areaStorageState: any = null

const areaStorageMock: any = {
    state: testRoute,
    save: (key, newState) => { areaStorageState = newState },
    load: (key) => of(areaStorageState)
}

describe("GameService", () => {
    it('create an instance', () => {
        const gameService = new GameService(routesServiceMock, elementsServiceMock, geolocationServiceMock, areaStorageMock);
        expect(gameService).toBeTruthy();
    })
})