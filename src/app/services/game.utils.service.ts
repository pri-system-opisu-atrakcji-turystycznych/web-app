import { GameProgress } from './game.model';
import { shuffle } from 'lodash';

export function createNewGame(route, element): GameProgress {
    return {
        route: route,
        targetElement: element,
        distanceToTarget: 0,
        targetIndex: 0,
        points: 0,
        question: null,
        status: 'onTheWay'
    }
}

export function createNotStartedGame(): GameProgress {
    return {
        route: null,
        targetElement: null,
        distanceToTarget: 0,
        targetIndex: 0,
        points: 0,
        question: null,
        status: 'notStarted'
    }
}

export function continueGame(progress: GameProgress, testAnswer: string): GameProgress {
    if(progress.status == 'targetReached') {
        return continueAfterTargetReached(progress);
    } 
    if(progress.status == 'test') {
        return continueAfterSolveTest(progress, testAnswer);
    }
    return progress;
}

function generateNewQuestion(progress: GameProgress): GameProgress {
    let elementQuestion = progress.targetElement.question;
    return {
        ...progress,
        question: {
            question: elementQuestion.content,
            answers: shuffle([
                elementQuestion.answerOne,
                elementQuestion.answerTwo,
                elementQuestion.answerTree,
                elementQuestion.rightAnswer
            ]),
            rightAnswer: elementQuestion.rightAnswer
        }
    }
}

function checkAnswer(progress: GameProgress, answer: string): GameProgress {
    return {
        ...progress,
        points: progress.points + (progress.question && progress.question.rightAnswer == answer ? 1 : 0)
    }
}

function setNextElement(progress: GameProgress): GameProgress {
    let elementCount = progress.route.elements.length
    let completed: boolean = elementCount == progress.targetIndex + 1
    return {
        ...progress,
        targetIndex: progress.targetIndex + (completed ? 0 : 1),
        status: completed ? 'completed' : 'onTheWay',
        targetElement: completed ? null : progress.targetElement
    }
}

function continueAfterTargetReached(progress: GameProgress): GameProgress {
    if(progress.question)
        return {...progress, status: 'test'}
    else {
        progress = generateNewQuestion(progress);
        progress = setNextElement(progress)
        return progress
    }
}

function continueAfterSolveTest(progress: GameProgress, testAnswer: string): GameProgress {
    progress = checkAnswer(progress, testAnswer);
    progress = generateNewQuestion(progress);
    progress = setNextElement(progress);
    return progress;
}