import { Injectable } from '@angular/core';
import { AreasStoreService } from '../modules/areas/areas-store.service';
import { Observable, EMPTY } from 'rxjs';
import { safeParse } from './storage.service';
import { take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AreaStorageService {

  constructor(private areaStore: AreasStoreService) {}

  save(key: string, data: Object): Observable<void> {
    return this.areaStore.currentArea$.pipe(take(1), map(currentArea => {
      localStorage.setItem(`${currentArea.name}_${key}`, JSON.stringify(data));
      return void 0;
    }));
  }

  load<T>(key): Observable<T> {
    return this.areaStore.currentArea$.pipe(map(currentArea => {
      return safeParse(localStorage.getItem(`${currentArea.name}_${key}`))
    }));
  }
}
