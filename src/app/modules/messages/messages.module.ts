import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesPageComponent } from './messages-page/messages-page.component';
import { RouterModule } from '@angular/router';

const routes = [
  {path: '', component: MessagesPageComponent},
]

@NgModule({
  declarations: [MessagesPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class MessagesModule { }
