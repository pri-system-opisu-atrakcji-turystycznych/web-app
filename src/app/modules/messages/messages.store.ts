import { MessagesService } from './messages.service';
import { Injectable } from '@angular/core';
import { timer, Observable, BehaviorSubject, NEVER } from 'rxjs';
import { Message } from './message.model';
import { switchMap, map, tap } from 'rxjs/operators';
import { AreasStoreService } from '../areas/areas-store.service';
import { StoragePath, storage } from 'src/app/services/storage.service';
import { head } from 'src/app/services/utils.service';

@Injectable({
    providedIn: 'root'
})
export class MessagesStore implements StoragePath {
    messages = new BehaviorSubject<Message[]>([])
    storagePath = null;
    @storage() lastReadMessageId: number = -1;

    constructor(private messagesService: MessagesService, private areasStore: AreasStoreService) {
        this.areasStore.currentArea$.pipe(
            tap(area => {if(area) this.storagePath = area.normalizedName}),
            switchMap(area => area === null ? NEVER : timer(0, 60 * 1000))
        ).subscribe(_ => this.refreshData());
    }

    refreshData() {
        this.messagesService.getMessages().subscribe(ms => this.messages.next(ms))
    }

    get messages$() {
        return this.messages.pipe( tap(ms => ms.forEach(m => m.read = m.id <= this.lastReadMessageId)) );
    }

    readMessages() {
        if(this.messages.value.length > 0) {
            this.lastReadMessageId = head(this.messages.value).id;
        }
        this.messages.next(this.messages.value)
    }

    getUnreadMessagesNumber(): Observable<number> {
        return this.messages$.pipe(map(ms => ms.filter(m => !m.read).length));
    }

}