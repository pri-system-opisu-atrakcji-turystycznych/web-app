import { Injectable } from '@angular/core';
import { Message } from './message.model';
import { AreaHttpService } from 'src/app/services/area-http.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private http: AreaHttpService) { }

  getMessages() {
    return this.http.get<Message[]>("messages").pipe(tap(ms => ms.reverse()));
  }
}
