import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { MessagesStore } from '../messages.store';

@Component({
  selector: 'app-messages-page',
  templateUrl: './messages-page.component.html',
  styleUrls: ['./messages-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessagesPageComponent implements OnInit, OnDestroy {
  messages$ = this.messagesStore.messages$;

  constructor(private messagesStore: MessagesStore) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.messagesStore.readMessages();
  }

}
