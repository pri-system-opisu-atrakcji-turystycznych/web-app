import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MapService } from '../../map/map.service';
import { GameProgress } from 'src/app/services/game.model';

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GamePageComponent implements OnInit, OnDestroy {
  gameProgress$: Observable<GameProgress> = this.gameService.progress$.pipe(
    tap(progress => this.mapService.updateSettings({
      route: progress.route, 
      element: progress.targetElement,
      links: false
    }))
  )
  myAnswer: string;

  constructor(
    private gameService: GameService,
    private mapService: MapService
  ) { }

  ngOnInit() {}

  ngOnDestroy() {
    this.mapService.updateSettings({route: null, element: null, links: true});
  }

  continue() {
    this.gameService.continue(this.myAnswer);
    this.myAnswer = null;
  }

  endGame() {
    this.gameService.endGame();
  }
}
