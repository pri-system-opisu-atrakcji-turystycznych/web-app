import { Injectable } from '@angular/core';
import { AreaHttpService } from '../../services/area-http.service';
import { Observable } from 'rxjs';
import { Institution } from './institution.model';

@Injectable({
  providedIn: 'root'
})
export class InstitutionService {

  constructor(private http: AreaHttpService) { }

  getInstitiution(): Observable<Institution> {
    return this.http.get<Institution>("institutions/main")
  }
}
