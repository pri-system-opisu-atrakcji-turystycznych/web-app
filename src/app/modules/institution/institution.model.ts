export class Institution {
    id: number;
    name: string;
    description: string;
    location: string;
    phone: string;
    email: string;
}