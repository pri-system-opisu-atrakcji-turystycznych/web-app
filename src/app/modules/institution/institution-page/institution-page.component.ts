import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { InstitutionService } from '../institution.service';
import { Observable } from 'rxjs';
import { Institution } from '../institution.model';

@Component({
  selector: 'app-institution-page',
  templateUrl: './institution-page.component.html',
  styleUrls: ['./institution-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionPageComponent implements OnInit {
  institution$: Observable<Institution>;

  constructor(private institutionService: InstitutionService) { }

  ngOnInit() {
    this.institution$ = this.institutionService.getInstitiution()
  }

}
