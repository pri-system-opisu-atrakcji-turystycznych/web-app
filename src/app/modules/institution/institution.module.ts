import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InstitutionPageComponent } from './institution-page/institution-page.component';
import { RouterModule } from '@angular/router';

const routes = [
  {path: '', component: InstitutionPageComponent},
]

@NgModule({
  declarations: [InstitutionPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class InstitutionModule { }
