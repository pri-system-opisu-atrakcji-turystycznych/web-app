import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap, switchMap, catchError } from 'rxjs/operators';
import { TouristElement, ElementDescription } from './tourist-element-model';
import { AreaHttpService } from 'src/app/services/area-http.service';
import { normalizeNames, findByName } from 'src/app/services/utils.service';

@Injectable({
  providedIn: 'root'
})
export class TouristElementsService {

  constructor(private http: AreaHttpService) { }

  getElements(): Observable<TouristElement[]> {
    return this.http.get<TouristElement[]>('elements').pipe(normalizeNames())
  }

  getElementDetail(name: string): Observable<TouristElement> {
    return this.getElements().pipe(
      findByName<TouristElement>(name),
      switchMap(e => this.getElementDescription(e.id).pipe(
        tap(des => e.description = des.content),
        map(des => e)
      )),
      tap(element => this.http.generatePhotoUrl(element.tip)),
      catchError(e => of(null))
    )
  }

  getElementDescription(id: number): Observable<ElementDescription> {
    return this.http.get<ElementDescription>(`descriptions/element/${id}/mobile/false/web/true`).pipe(
      tap(d => d.content = d.content.replace('&amp;', '&'))
    )
  }
}
