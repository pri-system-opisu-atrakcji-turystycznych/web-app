import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { TouristElementsPageComponent } from './tourist-elements-page/tourist-elements-page.component';

const routes = [
  { path: '', component: TouristElementsPageComponent },
  { path: '', loadChildren: '../tourist-element-detail/tourist-element-detail.module#TouristElementDetailModule' },
]


@NgModule({
  declarations: [
    TouristElementsPageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class TouristElementsModule { }
