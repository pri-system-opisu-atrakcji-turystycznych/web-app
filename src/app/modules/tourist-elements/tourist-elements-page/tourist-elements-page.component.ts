import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { TouristElementsStore } from '../tourist-elements.store';

@Component({
  selector: 'app-tourist-elements-page',
  templateUrl: './tourist-elements-page.component.html',
  styleUrls: ['./tourist-elements-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TouristElementsPageComponent implements OnInit {
  
  constructor(public touristElementsStore: TouristElementsStore) { }

  ngOnInit() {
  }

}
