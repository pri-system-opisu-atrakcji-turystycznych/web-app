export class TouristElement {
    id: number;
    name: string;
    gpsX: string;
    gpsY: string;
    shortDescription: string;
    description: string;
    tip: TipModel;
    question: Question;

    normalizedName: string;
    photoUrl: string;

}

export class ElementDescription {
    element: TouristElement;
    content: string;
}

export class TipModel {
    id: number;
    photo: number;
    photoUrl: number;
    description: string;
}

export class Question {
    id: number;
    content: string;
    answerOne: string;
    answerTree: string;
    answerTwo: string;
    rightAnswer: string;
}