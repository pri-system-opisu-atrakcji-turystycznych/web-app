import { Injectable } from '@angular/core';
import { TouristElementsService } from './tourist-elements.service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { TouristElement } from './tourist-element-model';
import { cache, normalStr, deepDistinct } from 'src/app/services/utils.service';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TouristElementsStore {
  private elementSelector = new BehaviorSubject<string>(null);
  readonly elements$: Observable<TouristElement[]> = this.touristElementsService.getElements().pipe(cache())
  readonly selectedElement$: Observable<TouristElement> = this.elementSelector.pipe(
    map(n => normalStr(n)), 
    deepDistinct(), 
    switchMap(n => n ? this.touristElementsService.getElementDetail(n) : of(null)), 
    cache()
  )

  constructor(private touristElementsService: TouristElementsService) {}

  selectElementByName(name) {
    this.elementSelector.next(name);
  }

  unselectElement() {
    this.elementSelector.next(null);
  }
}
