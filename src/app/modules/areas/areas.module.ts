import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaListComponent } from './area-list/area-list.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

const routes = [
  { path: 'areas', loadChildren: '../single-area/single-area.module#SingleAreaModule' }
]

@NgModule({
  declarations: [
    AreaListComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    SharedModule
  ],
  exports: [
    AreaListComponent,
    RouterModule
  ]
})
export class AreasModule { }
