import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { Area } from './area-model';
import { AreasService } from './areas.service';
import { take, switchMap, map, tap, catchError } from 'rxjs/operators';
import { MyHttpService } from 'src/app/services/my-http.service';
import { cache, deepDistinct } from 'src/app/services/utils.service';

@Injectable({
  providedIn: 'root'
})
export class AreasStoreService {
  private currentArea: BehaviorSubject<Area> = new BehaviorSubject(this.loadCurrentArea())
  private _areas: Observable<Area[]> = this.areasService.getAreas().pipe(cache(), deepDistinct())

  constructor(private areasService: AreasService, private http: MyHttpService) {}

  get areas$(): Observable<Area[]> {
    return this._areas
  }

  get currentArea$(): Observable<Area> {
    return this.currentArea.asObservable();
  }

  setCurrentAreaByNormalizedName(name: string): Observable<Area> {
    return this.areas$.pipe(take(1), switchMap(areas => 
        this.setCurrentArea(areas.find(a => a.normalizedName === name))
    ))
  }

  private setCurrentArea(area: Area): Observable<Area> {
    return this.http.get<string>(area.url + "server/status").pipe(
      catchError(a => {
        if(a.status === 200) return "work";
        else return throwError({message: "Server Unavailable", area: a});
      }),
      map(_ => area),
      tap(area => {
        this.currentArea.next(area);
        this.saveCurrentArea();
      })
    )
  }

  private loadCurrentArea(): Area {
    let area: Area;
    try {
      area = JSON.parse(localStorage.getItem('currentArea'))
    }
    finally {
      return area || null;
    }
  }

  private saveCurrentArea() {
    localStorage.setItem('currentArea', JSON.stringify(this.currentArea.getValue()));
  }
}
