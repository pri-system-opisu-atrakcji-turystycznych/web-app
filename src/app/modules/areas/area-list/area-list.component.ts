import { Component, OnInit } from '@angular/core';
import { AreasStoreService } from '../areas-store.service';

@Component({
  selector: 'app-area-list',
  templateUrl: './area-list.component.html',
  styleUrls: ['./area-list.component.css']
})
export class AreaListComponent implements OnInit {

  areas$ = this.areasService.areas$;

  constructor(private areasService: AreasStoreService) {
  }

  ngOnInit() {
  }

}
