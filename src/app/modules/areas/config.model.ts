import { Area } from './area-model';

export interface ConfigApp {
    defaultArea: Area,
    showOtherAreas: boolean
}