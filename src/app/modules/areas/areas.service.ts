import { Injectable } from '@angular/core';
import { MyHttpService } from 'src/app/services/my-http.service';
import { Area } from './area-model';
import { Observable, of } from 'rxjs';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import { normalStr } from 'src/app/services/utils.service';
import { HttpClient } from '@angular/common/http';
import { ConfigApp } from './config.model';

@Injectable({
    providedIn: 'root'
})
export class AreasService {

    constructor(private adminHttp: MyHttpService, private http: HttpClient) { }

    getAreas(): Observable<Area[]> {
        return this.http.get<ConfigApp>('assets/config.json').pipe(
            switchMap(config => {
                if(config.showOtherAreas) {
                    return this.getAdditionalAreas(config.defaultArea)
                } else {
                    return of([config.defaultArea])
                }
            }),
            tap(areas => areas.forEach(area => {area.normalizedName = normalStr(area.name)}))
        )
    }

    getAdditionalAreas(defaultArea: Area): Observable<Area[]> {
        return this.adminHttp.get<Area[]>('areas/web', { format: 'json' }).pipe(
            catchError(e => of([defaultArea]))
        )
    }
}
