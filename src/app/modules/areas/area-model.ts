export interface Area {
    name: string;
    url: string;
    normalizedName: string;
}

export class AreaNull implements Area {
    name = "";
    url = "";
    normalizedName = "";
}