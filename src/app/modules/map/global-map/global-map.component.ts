import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TouristElementsStore } from '../../tourist-elements/tourist-elements.store';
import { TouristElement } from '../../tourist-elements/tourist-element-model';
import { MapService } from '../map.service';
import { take, tap } from 'rxjs/operators';
import { GoelocationService } from 'src/app/services/goelocation.service';
import { MapComponent } from '../map/map.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-global-map',
  templateUrl: './global-map.component.html',
  styleUrls: ['./global-map.component.css']
})
export class GlobalMapComponent implements OnInit {
  @ViewChild("map") map: MapComponent;
  @ViewChild("errorModal") errorModal

  settings$ = this.mapService.settings$

  constructor(public touristElementsStore: TouristElementsStore, public mapService: MapService, 
    private activatedRoute: ActivatedRoute, private router: Router, public geolocation: GoelocationService, public modalService: NgbModal) {    
  }

  ngOnInit() {}

  onClickElement(element: TouristElement) {
    this.mapService.settings$.pipe(take(1)).subscribe(settings => {
      if(!settings.links) return;
      if(settings.route === null) {
        this.router.navigate(["elements", element.normalizedName], {relativeTo: this.activatedRoute});
      } else {
        this.router.navigate(["routes", settings.route.normalizedName, "elements", element.normalizedName], {relativeTo: this.activatedRoute});
      }
    })
  }

  onGeolocationClick() {
    this.geolocation.enable().pipe(take(1)).subscribe(data => {
      this.map.recenter(data.position);
    }, error => {
      this.modalService.open(this.errorModal);
    })
  }

  onAreaClick() {
    this.map.recenter(null);
  }

  onRouteClick() {
    this.map.centerRoute();
  }

  onTargetClick() {
    this.map.centerTarget();
  }
}
