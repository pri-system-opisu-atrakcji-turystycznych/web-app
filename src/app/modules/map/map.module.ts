import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MapComponent } from './map/map.component';
import { GlobalMapComponent } from './global-map/global-map.component';

@NgModule({
  declarations: [
    MapComponent, 
    GlobalMapComponent
  ],
  imports: [
    CommonModule,
    LeafletModule,
  ],
  exports: [
    GlobalMapComponent
  ]
})
export class MapModule { }
