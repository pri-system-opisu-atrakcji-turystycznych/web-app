import { LatLng } from 'leaflet';

export class GeolocationPos {
    pos: LatLng;
    radius: number;

    constructor(coords: Coordinates) {
        this.pos = new LatLng(coords.latitude, coords.longitude);
        this.radius = coords.accuracy/2;
    }
}