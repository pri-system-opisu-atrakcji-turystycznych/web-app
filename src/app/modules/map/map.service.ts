import { Injectable } from '@angular/core';
import { Route } from '../routes/route-model';
import { BehaviorSubject, Observable } from 'rxjs';
import { TouristElement } from '../tourist-elements/tourist-element-model';
import { ApproximatePosition } from 'src/app/services/goelocation.service';
import { map } from 'rxjs/operators';
import { deepDistinct } from 'src/app/services/utils.service';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private route: BehaviorSubject<Route> = new BehaviorSubject(null);
  private settings = new BehaviorSubject<MapSettings>(defaultMapSettings);

  constructor() { }

  updateSettings(settings: MapSettings) {
    this.settings.next({
      ...this.settings.value,
      ...settings
    });
    //console.log('newSettings', this.settings.value)
  }

  get settings$(): Observable<MapSettings> {
    return this.settings.asObservable().pipe(deepDistinct())
  }
}

export interface MapSettings {
  route?: Route;
  element?: TouristElement;
  location?: ApproximatePosition;
  links?:boolean
}

const defaultMapSettings: MapSettings = {
  route: null,
  element: null,
  location: null,
  links: true
}
