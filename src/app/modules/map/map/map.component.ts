import { Component, OnInit, Input, Output, EventEmitter, NgZone, ViewChild } from '@angular/core';
import { LatLng, latLng, tileLayer, marker, Marker, polyline, Map, circle, LatLngLiteral } from 'leaflet';
import { TouristElement } from '../../tourist-elements/tourist-element-model';
import { markerIcon, createMarkerIcon, createGeolocationMarker } from './marker-icons';
import { Route } from '../../routes/route-model';
import { ReplaySubject } from 'rxjs';
import { take, filter } from 'rxjs/operators';
import { GoelocationService, ApproximatePosition } from 'src/app/services/goelocation.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  map: ReplaySubject<Map> = new ReplaySubject();
  options: any = { 
    layers: [tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    })] 
  };
  _elements: TouristElement[] = []
  _selectedElement: TouristElement = null;
  _route: Route = null;
  markers: Array<Marker> = [];
  layers: Array<any> = [];
  _geolocationPos: ApproximatePosition
  lastHeight: number = 0;
  @ViewChild('map') m;

  constructor(private ngZone: NgZone, private geolocation: GoelocationService) { }

  ngOnInit() {
  }

  ngAfterContentChecked() {
    if (this.m.nativeElement.offsetHeight !== this.lastHeight) {
      if(this.m.nativeElement.offsetHeight > 0) {
        this.map.pipe(take(1)).subscribe(map => map.invalidateSize());
        this.lastHeight = this.m.nativeElement.offsetHeight;
      }
      this.updateBounds();
    }
  }

  onReady(map: Map) {
    this.map.next(map)
    map.doubleClickZoom.disable(); 
  }

  @Input() set elements(elements: TouristElement[]) {
    this._elements = elements;
    if (elements && elements.length > 0) {
      this.updateBounds();
      this.updateMarkers();
    }
  }

  @Output() selectedElementChange = new EventEmitter();

  @Input() set selectedElement(element: TouristElement) {
    if (this._selectedElement !== element) {
      this._selectedElement = element;
      this.updateBounds();
      this.updateMarkers();
    }
  }
  get selectedElement() {
    return new TouristElement()
  }

  @Input() set route(route: Route) {
    this._route = route;
    this.updateBounds();
    this.updateMarkers();
  }

  @Input() set geolocationCoords(coords: ApproximatePosition) {
    if(coords) this._geolocationPos = coords
    this.updateMarkers();
  }

  recenter(position: LatLng) {
    this.map.pipe(take(1), filter(map => !!map)).subscribe(map => {
        if(position === null) {
          map.fitBounds(<any>this._elements.map(e => new LatLng(+e.gpsX, +e.gpsY)), {animate: false})
        }
        else if(map.getCenter().equals(position, 0.001)) {
          map.fitBounds(<any>this._elements.map(e => new LatLng(+e.gpsX, +e.gpsY)).concat([position]), {animate: false})
        }
        else  {
          map.fitBounds(<any>[position], {animate: false})
        }
    })
  }

  private updateBounds() {
    this.map.pipe(take(1)).subscribe(map => {
      if(map) {
        if(this._route && !this._selectedElement) map.fitBounds(<any>this._route.elements.map(e => latLng(+e.gpsX, +e.gpsY)), {animate: false});
        else if(this._elements && !this._selectedElement) map.fitBounds(<any>this._elements.map(e => latLng(+e.gpsX, +e.gpsY)), {animate: false});
        if (this._elements && this._selectedElement)
          map.fitBounds(<any>[new LatLng(+this._selectedElement.gpsX, +this._selectedElement.gpsY)], {animate: false})
      }
    });
  }

  centerRoute() {
    this.map.pipe(take(1)).subscribe(map => {
      map.fitBounds(<any>this._route.elements.map(e => latLng(+e.gpsX, +e.gpsY)), {animate: false});
    });
  }

  centerTarget() {
    this.map.pipe(take(1)).subscribe(map => {
      map.fitBounds(<any>[new LatLng(+this._selectedElement.gpsX, +this._selectedElement.gpsY)], {animate: false})
    });
  }

  private updateMarkers() {
    if (this._elements) {
      if (this.markers.length === 0) {
        this.markers = this._elements.map(e => marker([+e.gpsX, +e.gpsY])
          .addEventListener('click', _ => this.ngZone.run(_ => {
            if (this._selectedElement !== e) {
              this.selectedElementChange.emit(e);
            }
          }))
        )
      }
      
      this.markers.forEach((marker, index) => {
        let onRouteIndex = -1;
        marker.setIcon(markerIcon)
        let selected = this._selectedElement && this._elements[index].id === this._selectedElement.id;
        if (this._route) {
          onRouteIndex = this._route.elements.findIndex(el => el.id === this._elements[index].id)
        }
        let onRoute = onRouteIndex > -1;
        if (onRoute || selected) marker.setIcon(createMarkerIcon(selected, onRoute, onRouteIndex + 1))
      });
      this.layers = [...this.markers];
      if (this._route) this.layers.push(polyline(this._route.elements.map(e => latLng(+e.gpsX, +e.gpsY)), {dashArray: "5 5"}))
      if (this._geolocationPos) {
        this.layers.push(circle(this._geolocationPos.position, this._geolocationPos.radius, {color: "#c46363" }))
        this.layers.push(marker(this._geolocationPos.position, {icon: createGeolocationMarker()}))
      }
    }
  }

  public onClick(event) {
    if ((<any>window).test) {
      this.geolocation.setCoords({position: event.latlng, radius: 1})
    }
  }
  
}
