import { divIcon } from "leaflet";

export const markerIcon = divIcon({
    className: "marker-container",
    iconAnchor: [0, 24],
    popupAnchor: [0, -36],
    html: `<span class="marker"/>`
})

export function createGeolocationMarker(){
    return divIcon({
        className: "marker-container",
        iconAnchor: [0, 24],
        popupAnchor: [0, -36],
        html: `<span class="marker marker-geolocation"/>`
    }) 
}

export function createMarkerIcon(selected: boolean, onRoute: boolean, index: number) {
    let markerClass: string = "";
    if(selected) markerClass += " selected-marker";
    if(onRoute) markerClass += " route-marker";
    let text = onRoute ? index : ""

    return divIcon({
        className: "marker-container",
        iconAnchor: [0, 24],
        popupAnchor: [0, -36],
        html: `<span class="marker ${markerClass}"></span><span class="marker-text">${text}</span>`
    })
}


