import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { RoutesService } from '../routes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, tap, filter, take } from 'rxjs/operators';
import { Route } from '../route-model';
import { MapService } from '../../map/map.service';
import { GameService } from '../../../services/game.service';
import { GameProgress } from '../../../services/game.model';
import { AreaRoute } from '../../single-area/single-area-page/single-area-page.component';
import { untilDestroyed } from '@orchestrator/ngx-until-destroyed';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-route-detail-page',
  templateUrl: './route-detail-page.component.html',
  styleUrls: ['./route-detail-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RouteDetailPageComponent implements OnInit {
  route$: Observable<Route>
  @ViewChild('continueModel') continueModel

  constructor(
    private routesService: RoutesService, 
    private activatedRoute: ActivatedRoute, 
    private mapService: MapService,
    private gameService: GameService,
    private router: Router,
    private areaRoute: AreaRoute,
    private modal: NgbModal
  ) {}

  ngOnInit() {
    this.route$ = this.activatedRoute.paramMap.pipe(
      switchMap(p => this.routesService.getRouteDetail( p.get("name") )),
    )
    this.gameService.events$.pipe(filter(e => e == 'gameStarted'), untilDestroyed(this)).subscribe(e => {
      this.router.navigate(['game'], {relativeTo: this.areaRoute});
    });
  }

  startGame(routeName) {
    this.gameService.gameProgress.pipe(take(1), switchMap(progress => {
      if(progress.status == 'notStarted') return of(void 0)
      else return from(this.modal.open(this.continueModel).result)
    })).subscribe(data => {
      this.gameService.startGame(routeName);
    }, error => {});
  }

  ngOnDestroy() {

  }
}
