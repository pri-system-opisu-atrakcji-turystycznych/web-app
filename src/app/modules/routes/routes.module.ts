import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutesListComponent } from './routes-list/routes-list.component';
import { RouterModule } from '@angular/router';
import { RouteDetailPageComponent } from './route-detail-page/route-detail-page.component';
import { RouteOnMapComponent } from './route-on-map/route-on-map.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

const routes = [
  {path: '', component: RoutesListComponent},
  {path: ':name', component: RouteOnMapComponent, children: [
    {path: '', component: RouteDetailPageComponent},
    {path: 'elements', loadChildren: '../tourist-element-detail/tourist-element-detail.module#TouristElementDetailModule' }
  ]}
]

@NgModule({
  declarations: [
    RoutesListComponent,
    RouteDetailPageComponent,
    RouteOnMapComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FontAwesomeModule
  ]
})
export class RoutesModule { }
