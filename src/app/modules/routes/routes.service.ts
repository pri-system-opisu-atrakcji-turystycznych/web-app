import { Injectable } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { Route, RouteElements } from './route-model';
import { tap, map, switchMap } from 'rxjs/operators';
import { normalStr } from '../../services/utils.service';
import { TouristElement } from '../tourist-elements/tourist-element-model';
import { AreaHttpService } from 'src/app/services/area-http.service';

@Injectable({
  providedIn: 'root'
})
export class RoutesService {

  constructor(private http: AreaHttpService) { }

  getRoutes(): Observable<Route[]> {
    return this.http.get<Route[]>('routes').pipe(tap(es => es.forEach(e => e.normalizedName = normalStr(e.name))))
  }

  getRouteDetail(name): Observable<Route> {
    const route$ = this.getRouteByName(name)
    const elements$ = route$.pipe(switchMap(route => this.getRouteTouristElement(route.id)))
    return combineLatest(route$, elements$).pipe(map(([route, elements]) => ({
        ...route,
        elements: elements
      })
    ));
  }

  private getRouteByName(name: string): Observable<Route> {
    return this.getRoutes().pipe(map(rs => rs.find(r => r.normalizedName === normalStr(name))))
  }

  private getRouteTouristElement(id: number): Observable<TouristElement[]> {
    return this.http.get<RouteElements[]>(`routes-elements/route/${id}`).pipe(
      map(res => res.map(re => re.element)),
      tap(elements => elements.forEach(b => b.normalizedName = normalStr(b.name)) ),
      tap(elements => this.http.generatePhotoUrl(elements))
    )
  }
}
