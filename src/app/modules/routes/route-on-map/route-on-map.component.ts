import { Component, OnInit, OnDestroy } from '@angular/core';
import { RoutesService } from '../routes.service';
import { ActivatedRoute } from '@angular/router';
import { MapService } from '../../map/map.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-route-on-map',
  templateUrl: './route-on-map.component.html',
  styleUrls: ['./route-on-map.component.css']
})
export class RouteOnMapComponent implements OnInit, OnDestroy {

  constructor(private routesService: RoutesService, private activatedRoute: ActivatedRoute, private mapService: MapService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.pipe(
      switchMap(p => this.routesService.getRouteDetail( p.get("name") )),
    ).subscribe(route => this.mapService.updateSettings({route: route}))
  }

  ngOnDestroy() {
    this.mapService.updateSettings({route: null});
  }

}
