import { TouristElement } from '../tourist-elements/tourist-element-model';

export class Route {
    public id: number;
    public name: string;
    public description: string;
    public normalizedName: string;
    public elements: TouristElement[];
}

export class RouteElements {
    element: TouristElement;
    elementModel: TouristElement;
}