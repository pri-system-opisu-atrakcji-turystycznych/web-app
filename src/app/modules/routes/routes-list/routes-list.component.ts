import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { RoutesService } from '../routes.service';

@Component({
  selector: 'app-routes-list',
  templateUrl: './routes-list.component.html',
  styleUrls: ['./routes-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoutesListComponent implements OnInit {
  routes$ = this.routesService.getRoutes();

  constructor(private routesService: RoutesService) { }

  ngOnInit() {
  }

}
