import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'distance'
})
export class DistancePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value >= 1000) {
      return `${Math.round(value/1000)} km.`
    } else {
      return `${Math.round(value)} m.`
    }
  }

}
