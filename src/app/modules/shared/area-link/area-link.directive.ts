import { Directive, Input, OnDestroy } from '@angular/core';
import { RouterLinkWithHref, Router, ActivatedRoute } from '@angular/router';
import { LocationStrategy } from '@angular/common';
import { AreasStoreService } from '../../areas/areas-store.service';
import { take } from 'rxjs/operators';
import { BehaviorSubject, Subject, combineLatest } from 'rxjs';
import { untilDestroyed } from '@orchestrator/ngx-until-destroyed';

@Directive({
  selector: 'a[areaLink]',
  providers: [
    {provide: RouterLinkWithHref, useExisting: AreaLinkDirective}
  ]
})
export class AreaLinkDirective extends RouterLinkWithHref implements OnDestroy {
  private link$ = new Subject()

  constructor(
    router: Router, 
    activatedRoute: ActivatedRoute,
    locationStrategy: LocationStrategy,
    private areasStore: AreasStoreService
  ) {
    super(router, activatedRoute, locationStrategy)
    combineLatest(
      this.link$,
      this.areasStore.currentArea$
    ).pipe(untilDestroyed(this)).subscribe(([link, currentArea]) => {
      this.routerLink = `/areas/${currentArea.normalizedName}/${link}`;
    })
  }

  @Input('areaLink') set link(link: string) {
    this.link$.next(link);
  }

  ngOnDestroy() {}
}
