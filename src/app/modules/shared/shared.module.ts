import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaLinkDirective } from './area-link/area-link.directive';
import { DistancePipe } from './pipes/distance.pipe';

@NgModule({
  declarations: [
    AreaLinkDirective,
    DistancePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AreaLinkDirective,
    DistancePipe
  ]
})
export class SharedModule { }
