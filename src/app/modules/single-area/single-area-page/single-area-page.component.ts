import { Component, OnInit } from '@angular/core';
import { AreasStoreService } from '../../areas/areas-store.service';
import { ActivatedRoute } from '@angular/router';

export class AreaRoute extends ActivatedRoute {}

@Component({
  selector: 'app-single-area-page',
  templateUrl: './single-area-page.component.html',
  styleUrls: ['./single-area-page.component.css'],
  providers: [
    {provide: AreaRoute, useExisting: ActivatedRoute}
  ]
})
export class SingleAreaPageComponent implements OnInit {

  constructor(public areasStore: AreasStoreService, public activatedRoute: ActivatedRoute) {}

  ngOnInit() {}

  activeView: 'content' | 'map' = 'content';

}
