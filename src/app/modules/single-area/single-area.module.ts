import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapModule } from '../map/map.module';
import { RouterModule, Routes } from '@angular/router';
import { SingleAreaPageComponent } from './single-area-page/single-area-page.component';
import { AreaAvailableGuard } from './area-available.guard';
import { GamePageComponent } from '../game/game-page/game-page.component';
import { SharedModule } from '../shared/shared.module';
import { RadioButtonModule } from 'primeng/radiobutton';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: ':name', component: SingleAreaPageComponent, canActivate: [AreaAvailableGuard], children: [
    { path: '', redirectTo: 'routes', pathMatch: 'full'},
    { path: 'routes', loadChildren: '../routes/routes.module#RoutesModule' },
    { path: 'elements', loadChildren: '../tourist-elements/tourist-elements.module#TouristElementsModule' },
    { path: 'messages', loadChildren: '../messages/messages.module#MessagesModule' },
    { path: 'institution', loadChildren: '../institution/institution.module#InstitutionModule' },
    { path: 'game', component: GamePageComponent }
  ] },
]

@NgModule({
  declarations: [
    SingleAreaPageComponent,
    GamePageComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MapModule,
    SharedModule,
    FormsModule,
    RadioButtonModule
  ]
})
export class SingleAreaModule { }
