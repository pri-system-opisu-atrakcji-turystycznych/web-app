import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AreasStoreService } from '../areas/areas-store.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AreaAvailableGuard implements CanActivate {
  constructor(private areasStore: AreasStoreService, private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, _: RouterStateSnapshot): Observable<boolean> {
    return this.areasStore.setCurrentAreaByNormalizedName(next.paramMap.get("name")).pipe(
      map(a => true),
      catchError(e => {
        this.router.navigate(["/"], {fragment: e})
        return of(false);
      })
    )
  }
}
