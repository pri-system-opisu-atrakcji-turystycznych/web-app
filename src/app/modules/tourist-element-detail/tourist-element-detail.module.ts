import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TouristElementDetailPageComponent } from './tourist-element-detail-page/tourist-element-detail-page.component';
import { RouterModule } from '@angular/router';

const routes = [
  { path: ':name', component: TouristElementDetailPageComponent }
]

@NgModule({
  declarations: [
    TouristElementDetailPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class TouristElementDetailModule { }
