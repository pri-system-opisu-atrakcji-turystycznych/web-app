import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TouristElementsStore } from '../../tourist-elements/tourist-elements.store';
import { MapService } from '../../map/map.service';
import { map, filter, tap } from 'rxjs/operators';
import { combineLatest, Observable } from 'rxjs';
import { TouristElement } from '../../tourist-elements/tourist-element-model';

@Component({
  selector: 'app-tourist-element-detail-page',
  templateUrl: './tourist-element-detail-page.component.html',
  styleUrls: ['./tourist-element-detail-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TouristElementDetailPageComponent implements OnInit, OnDestroy {
  routeElements$: Observable<any>
  selectedElement$: Observable<TouristElement> = this.touristElementsStore.selectedElement$.pipe(
    tap(element => this.mapService.updateSettings({element: element}))
  )
  
  constructor(
    public touristElementsStore: TouristElementsStore,
    private mapService: MapService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe( params => {
      this.touristElementsStore.selectElementByName(params.get("name"))
    });
    this.routeElements$ = combineLatest(
      this.mapService.settings$.pipe(map(settings => settings.route), filter(r => !!r)),
      this.touristElementsStore.selectedElement$.pipe(filter(r => !!r))
    ).pipe(map(([route, element]) => {
      let index = route.elements.findIndex(e => e.id === element.id);
      return {
        prev: index - 1 >= 0 ? route.elements[index - 1] : null,
        next: index + 1 < route.elements.length && index >= 0 ? route.elements[index + 1] : null
      } 
    })
    )
  }

  ngOnDestroy() {
    this.touristElementsStore.unselectElement();
    this.mapService.updateSettings({element: null})
  }

}
